require("dotenv").config();

const express = require("express");
const cors = require("cors");
const auth = require("./App/middleware/auth");
const bodyParser = require("body-parser");

const app = express();
const corsoption = {
    origin: "*",
};

app.use(cors(corsoption));
app.use(express.json());
app.use("/app/upload/", express.static('App/upload'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
    console.log("HTTP Method " + req.method + ", URL -" + req.url);
    next();
});

app.get("/", (req, resp) => {
    resp.json({ message: "hello from api " });
});

require('../Backend/App/route_controller')(app);

const PORT = process.env.PORT || 8087;
app.listen(PORT, () => {
    console.log(`server is running ${PORT}`);
});
