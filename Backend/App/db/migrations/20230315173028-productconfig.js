"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("ProductConfig", {
            id: {
                type: Sequelize.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true,
            },
            productId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "product",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            variationOptionId: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: true,
                references: {
                    model: "variationOption",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            createdBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            updatedBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            deletedBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("ProductConfig");
    },
};
