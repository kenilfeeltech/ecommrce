"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable("Product", {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            title: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            image: {
                type: Sequelize.STRING,
            },
            categoryId: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: true,
                references: {
                    model: "category",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            price: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            stock: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            shortDescription: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            longDescription: {
                type: Sequelize.STRING,
                allowNull: false,
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            deletedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            createdBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            updatedBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
            deletedBy: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                    model: "userAdmin",
                    key: "id",
                },
                onDelete: "CASCADE",
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable("Product");
    },
};
