module.exports = (sequelize, DataTypes) => {
    const ProductVariation = sequelize.define(
        "ProductVariation",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "productVariation",
        }
    );
    ProductVariation.associate = (models) => {
        ProductVariation.belongsTo(models.Product, {
            foreignKey: { name: "productId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
        ProductVariation.belongsTo(models.Variation, {
            foreignKey: { name: "variationId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });

        ProductVariation.belongsTo(models.UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        ProductVariation.belongsTo(models.UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        ProductVariation.belongsTo(models.UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });
    };
    return ProductVariation;
};
