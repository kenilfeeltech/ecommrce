module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define(
        "Category",
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: "category",
        }
    );
    Category.associate = (models) => {
        Category.belongsTo(models.UserAdmin, {
            foreignKey: { name: "createdBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "createdByUser",
        });
        Category.belongsTo(models.UserAdmin, {
            foreignKey: { name: "updatedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "updatedByUser",
        });
        Category.belongsTo(models.UserAdmin, {
            foreignKey: { name: "deletedBy", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
            as: "deletedByUser",
        });

        Category.hasMany(models.Product, {
            foreignKey: { name: "categoryId", allowNull: true },
            onUpdate: "CASCADE",
            onDelete: "RESTRICT",
        });
    };
    return Category;
};
