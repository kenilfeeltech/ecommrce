const express = require("express");
const variationOptionRouter = express.Router();
// const { auth } = require("../../middleware/
const {
    createVariationOption,
    updateVariationOption,
    deleteVariationOption,
    getOneVariationOption,
    getAllVariationOption,
} = require("./lib/controllers");



module.exports = (app) => {
   
    app.get("/variationoption/allVariation", getAllVariationOption);
app.get("/variationoption/:id", getOneVariationOption);
app.post("/variationoption", createVariationOption);   
app.delete("/variationoption/:id", deleteVariationOption);
app.put("/variationoption/:id", updateVariationOption);
}
