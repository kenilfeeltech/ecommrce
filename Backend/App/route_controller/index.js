module.exports = (app) => {
    require('./User')(app);
    require('./Useradmin')(app);
    require('./Role')(app);
    require('./Checkout')(app);
    require('./Category')(app);
    require('./Product')(app);
    require('./Varation')(app);
    require('./Varationoption')(app);
    require('./Productconfig')(app);
    require('./ProductVariation')(app);
};
