const express = require("express");
const userRouter = express.Router();
const { auth } = require("../../middleware/auth");
const {
    signIn,
    updateUser,
    deleteUser,
    getUsers,
    getUserById,
    addUser,
    //     upload,
} = require("../../route_controller/user/lib/controller");

// const { validation, signInvalidation } = require("./lib/validation");



module.exports = (app) => {

    app.get("/allUser", getUsers);
app.get("/user/:id",  getUserById);
app.post("/user/addUser", addUser);
app.post("/user/signIn", signIn);
app.put("/user/:id",updateUser);
app.delete("/user/:id", deleteUser);
}
