const express = require("express");
const { auth } = require("../../middleware/auth");
const checkoutRouter = express.Router();
const {
    createCheckout,
    updateCheckout,
    deleteCheckout,
    getOneCheckout,
    getAllCheckout,
} = require("../../route_controller/Checkout/lib/controllers");

checkoutRouter.get("/allCheckout", getAllCheckout);
checkoutRouter.get("/:id", getOneCheckout);
checkoutRouter.post("/addCheckout", createCheckout);
checkoutRouter.delete("/:id", deleteCheckout);
checkoutRouter.put("/:id", updateCheckout);

module.exports = (app) => {
    app.get("/checkout/allCheckout", getAllCheckout);
app.get("/checkout/:id", getOneCheckout);
app.post("/checkout/addCheckout", createCheckout);
app.delete("/checkout/:id", deleteCheckout);
app.put("/checkout/:id", updateCheckout);
}
