const express = require("express");
const productVariationRouter = express.Router();
const { auth } = require("../../middleware/auth");
const {
    getAllProductVariation,
    getOneProductVariation,
    createProductVariation,
    deleteProductVariation,
    updateProductVariation,
} = require("./lib/controllers");



module.exports = (app) => {
app.get("/productVariation/allProductVariation", getAllProductVariation);
app.get("/productVariation/:id", auth, getOneProductVariation);
app.post("/productVariation", auth, createProductVariation);
app.delete("/productVariation/:id", auth, deleteProductVariation);
app.put("/productVariation/:id", auth, updateProductVariation);
}
