const db = require("../../../db/models");

const VariationOption = db.VariationOption;
const Product = db.Product;
const Variation = db.Variation;
const ProductVariation = db.ProductVariation;

const createProductVariation = async (req, res) => {
    try {
        const { productId, variationId } = req.body;

        const productVariation = await ProductVariation.create({
            productId: productId,
            variationId: variationId,
        });
        return res.status(201).json({ data: productVariation });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: "something went wrong!" });
    }
};

const updateProductVariation = async (req, res) => {
    let id = req.params.id;

    const { productId, variationId } = req.body;
    const update = {
        productId: productId,
        variationId: variationId,
    };

    try {
        await ProductVariation.update(update, { where: { id: id } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
};

const deleteProductVariation = async (req, res) => {
    let id = req.params.id;
    const { productId, variationId } = req.body;
    const update = {
        productId: productId,
        variationId: variationId,
    };

    try {
        await ProductVariation.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: "something went wrong!" });
    }
    // await Role.destroy({where:{id:id,deletedby:null}})
    // res.status(200).send('DELETED')
};

const getOneProductVariation = async (req, res) => {
    let id = req.params.id;
    let catagory = await ProductVariation.findOne({
        where: { id: id },
    });
    return res.status(200).send(catagory);
};

const getAllProductVariation = async (req, res) => {
    const variation = await ProductVariation.findAll({
        include: [
            {
                model: Variation,
            },
        ],
        // include: [
        //     {
        //         model: VariationOption,
        //         as: Variation,
        //     },
        // ],
    });
    return res.status(200).send(variation);
};

module.exports = {
    createProductVariation,
    updateProductVariation,
    deleteProductVariation,
    getOneProductVariation,
    getAllProductVariation,
};
