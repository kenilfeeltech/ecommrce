const express = require("express");
const roleRouter = express.Router();
const { auth } = require("../../middleware/auth");
const {
    getAllRole,
    createRole,
    deleteRole,
    updateRole,
    getOneRole,
    // findAllStatus,
} = require("../Role/lib/controllers");



module.exports = (app) => {

    app.get("/role/allRole", getAllRole);
app.get("/role/:id", getOneRole);
app.post("/role/addRole", createRole);
app.delete("/role/:id", deleteRole);
app.put("/role/:id", updateRole);
}
