const express = require("express");
const productConfigRouter = express.Router();
const { auth } = require("../../middleware/auth");
const {
    createProductConfig,
    updateProductConfig,
    deleteProductConfig,
    getOneProductConfig,
    getAllProductConfig,
} = require("./lib/controllers");



module.exports = (app) => {
app.get("/config/allConfig", getAllProductConfig);
app.get("/config/:id", getOneProductConfig);
app.post("/config", createProductConfig);
app.delete("/config/:id", deleteProductConfig);
app.put("/config/:id", updateProductConfig);
}