const express = require("express");
// const catagoryRouter = express.Router();
const { auth } = require("../../middleware/auth");
const {
    createCategory,
    updateCategory,
    deleteCategory,
    getOneCategory,
    getAllCategory,
} = require("./lib/controllers");



module.exports = (app) => {
app.get("/category/", getAllCategory);
app.get("/category/:id", getOneCategory);
app.post("/category", createCategory);
app.delete("/category/:id", auth, deleteCategory);
app.put("/category/:id", auth, updateCategory);
}
