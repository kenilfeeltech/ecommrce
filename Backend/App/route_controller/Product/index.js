const express = require("express");
// const { auth } = require("../../middleware/auth");
const productRouter = express.Router();
const {
    createProduct,
    updateProduct,
    deleteProduct,
    getOneProduct,
    getAllProducts,
    upload,
} = require("../../route_controller/Product/lib/controllers");



module.exports = (app) => {
app.get("/product/", getAllProducts);
app.get("/product/:id", getOneProduct);
app.post("/product/", upload, createProduct);
app.delete("/product/:id", deleteProduct);
app.put("/product/:id", updateProduct);
}
