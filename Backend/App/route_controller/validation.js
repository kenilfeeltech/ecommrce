const { check } = require('express-validator');

exports.signinValidation = [
    check('email', 'email is required').isEmail(),
    check('password', 'please enter 1 to 6 length password').isLength({ min: 6 }),
];

exports.addValidation = [
    check('userName', 'enter the name').not().isEmpty(),
    // check('lastName', 'enter the name').not().isEmpty(),
    check('email', 'email is required').isEmail(),
    check('password', 'please enter 1 to 6 length password').isLength({ min: 6 }),
];
