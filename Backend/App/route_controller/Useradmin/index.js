const express = require("express");
// const userAdminRouter = express.Router();
// const { auth } = require("../../middleware/auth");
const {
    signIn,
    addUser,
    updateUser,
    deleteUser,
    getUsers,
    getUserById,
    //     upload,
} = require("./lib/controllers");

// const { validation, signInvalidation } = require("./lib/validation");


module.exports = (app) => {

    app.get("/useradmin/allUser", getUsers);
app.get("/useradmin/:id", getUserById);
app.post("/useradmin", addUser);
app.post("/useradmin/signIn", signIn);
app.put("/useradmin/:id", updateUser);
app.delete("/useradmin/:id", deleteUser);
}
