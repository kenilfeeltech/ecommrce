import axios from 'axios'
import React, { useState } from 'react'

import { Card, CardHeader, CardTitle, CardBody, Input, Form, Button, Row, Col, Label } from 'reactstrap'

function Addcategory() {

        const [name, setName] = useState("")

    const addcategory = async (event) => {
        event.preventDefault()

        try {
            const response = await axios.post(
                "http://localhost:8087/category",
                {
                    name
                }
            )
            console.log(response.data)
        } catch (error) {
            console.log(error)
        }
    }
  return (
    <div>
          <div>
<Card>
      <CardHeader>
        <CardTitle tag='h4'>Category Details</CardTitle>
      </CardHeader>

      <CardBody>
        <Form>
          <Row>
            <Col md='6' sm='12' className='mb-1'>
              <Label className='form-label' for='nameMulti'>
                Category
              </Label>
              <Input type='text' name='name' id='nameMulti' placeholder='Category' onChange={(e) => {
                setName(e.target.value)
                }}
                />
            </Col>
            <Col sm='12'>
              <div className='d-flex mt-2'>
                <Button className='me-1' color='primary' type='submit' onClick={addcategory}>
                  Submit
                </Button>
                <Button outline color='secondary' type='reset'>
                  Reset
                </Button>
              </div>
            </Col>
          </Row>
        </Form>
      </CardBody>
    </Card>
    </div>
    </div>
  )
}

export default Addcategory