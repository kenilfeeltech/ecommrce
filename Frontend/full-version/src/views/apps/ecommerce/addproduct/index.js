// ** React Imports
import { Fragment } from 'react'
// import { useParams } from 'react-router-dom'

// ** Product detail components
import Addproduct from './addproduct'

// ** Custom Components
import BreadCrumbs from '@components/breadcrumbs'

// ** Reactstrap Imports
// import { Card, CardBody } from 'reactstrap'

// ** Store & Actions
// import { useDispatch, useSelector } from 'react-redux'
// import { getProduct, deleteWishlistItem, addToWishlist, addToCart } from '../store'

import '@styles/base/pages/app-ecommerce-details.scss'

const Details = () => {
  // ** Vars
//   const params = useParams().product
//   const productId = params.substring(params.lastIndexOf('-') + 1)

//   // ** Store Vars
//   const dispatch = useDispatch()
//   const store = useSelector(state => state.ecommerce)

//   // ** ComponentDidMount : Get product
//   useEffect(() => {
//     dispatch(getProduct(productId))
//   }, [])

  return (
    <Fragment>
      <BreadCrumbs title='Add Product' data={[{ title: 'eCommerce' }, { title: 'Add Product' }]} />
     <Addproduct/>
    </Fragment>
  )
}

export default Details
