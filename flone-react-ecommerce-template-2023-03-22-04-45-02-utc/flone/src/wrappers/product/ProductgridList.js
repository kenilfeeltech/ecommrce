import PropTypes from "prop-types";
import React, { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import ProductGridListSingle from "../../components/product/ProductGridListSingle";
import axios from "../../service/axios-original";

const ProductGridList = ({
  products,
  spaceBottomClass
}) => {
  const currency = useSelector((state) => state.currency);
  const { cartItems } = useSelector((state) => state.cart);
  const { wishlistItems } = useSelector((state) => state.wishlist);
  const { compareItems } = useSelector((state) => state.compare);

  const [item, setItem] = useState([]);
    const getData = async (data) => {
        return await axios.get("product/", data).then((res) => {
            setItem(res.data);
            // console.log("---------", res.data);
        });
    };
    useEffect(() => {
        getData();
    }, []);
  
  return (
    <Fragment>
      {item?.map((product1) => {
        return (
          <div className="col-xl-4 col-sm-6" key={product1.id}>
            <ProductGridListSingle
              spaceBottomClass={spaceBottomClass}
              product={product1}
              currency={currency}
              cartItem={
                cartItems.find(cartItem => cartItem.id === product1.id)
              }
              wishlistItem={
                wishlistItems.find(
                  wishlistItem => wishlistItem.id === product1.id
                )
              }
              compareItem={
                compareItems.find(
                  compareItem => compareItem.id === product1.id
                )
              }
            />
          </div>
        );
      })}
    </Fragment>
  );
};

ProductGridList.propTypes = {
  products: PropTypes.array,
  spaceBottomClass: PropTypes.string,
};

export default ProductGridList;
