import { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
// import { getProducts } from "../../helpers/product";
import ProductGridSingle from "../../components/product/ProductGridSingle";
import axios from "../../service/axios-original";

const ProductGrid = () => {
    // const { products } = useSelector((state) => state.product);
    const currency = useSelector((state) => state.currency);
    const { cartItems } = useSelector((state) => state.cart);
    const { wishlistItems } = useSelector((state) => state.wishlist);
    const { compareItems } = useSelector((state) => state.compare);
    // const prods = getProducts(products, category, type, limit);

    const [item, setItem] = useState([]);
    const getData = async (data) => {
        return await axios.get("product/", data).then((res) => {
            setItem(res.data);
            // console.log(typeof res.data);
        });
    };
    useEffect(() => {
        getData();
    }, []);

    return (
        <Fragment>
            {item?.map((product1) => {
                // console.log(typeof product1)
                return (
                    <div
                        className="col-xl-3 col-md-6 col-lg-4 col-sm-6"
                        key={product1.id}
                    >
                        <ProductGridSingle
                            // spaceBottomClass={spaceBottomClass}
                            product={product1}
                            currency={currency}
                            cartItem={cartItems.find(
                                (cartItem) => cartItem.id === product1.id
                            )}
                            wishlistItem={wishlistItems.find(
                                (wishlistItem) =>
                                    wishlistItem.id === product1.id
                            )}
                            compareItem={compareItems.find(
                                (compareItem) => compareItem.id === product1.id
                            )}
                        />
                    </div>
                );
            })}
        </Fragment>
    );
};

ProductGrid.propTypes = {
    spaceBottomClass: PropTypes.string,
    category: PropTypes.string,
    type: PropTypes.string,
    limit: PropTypes.number,
};

export default ProductGrid;