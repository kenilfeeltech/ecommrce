import PropTypes from "prop-types";
import React, { Fragment, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getProductCartQuantity } from "../../helpers/product";
import Rating from "./sub-components/ProductRating";
import { addToCart } from "../../store/slices/cart-slice";
import { addToWishlist } from "../../store/slices/wishlist-slice";
import { addToCompare } from "../../store/slices/compare-slice";
// import axios from "../../service/axios-original";

const ProductDescriptionInfo = ({
  product,
  discountedPrice,
  currency,
  finalDiscountedPrice,
  finalProductPrice,
  cartItems,
  wishlistItem,
  compareItem,
}) => {
  const dispatch = useDispatch();
  const [selectedProductColor, setSelectedProductColor] = useState(
   
  );
  const [selectedProductSize, setSelectedProductSize] = useState(
   
  );
  const [productStock, setProductStock] = useState(
   
  ); 
  const [quantityCount, setQuantityCount] = useState(1);

  const productCartQty = getProductCartQuantity(
    cartItems,
    product,
    selectedProductColor,
    selectedProductSize
  );

  // console.log("product", product.ProductVariations.forEach((i) => {
  //   i.variationOption.forEach((k) => {
  //     console.log(k.name)
  //   })
  // }))
 
  // product?.ProductVariations.forEach((i) => {
  //   i?.variationOption.forEach((k) => {
  //     console.log("------", k.variationId)
  //   })
  // })

  const temp1 = []
  const temp2 = []
  const temp3 = []
  // const temp3 = []

  // product?.ProductVariations?.forEach((c) => {
  //       c?.variationOption.forEach((vo) => {   
  //           temp2.push({value : c.id , label : c.name})
  //           console.log("temp222222222",c)
  //           if (c.variationId === vo.variationId){
  //               temp3.filter(c.variationId === vo.variationId)
  //               console.log("====333===",temp3)
  //           }    
  //       });
  //   });

  product?.ProductVariations?.forEach((c) => {
        var newItem = c.variationOption.filter(
            (word) => !product?.ProductVariations.includes(word)
        );

        if (newItem[0].variationId === 1) {
            temp1.push(newItem);
        } else {
            temp2.push(newItem);
        }
        // console.log("1111111111111",temp1)
        // console.log("2222222222222",temp2)
    });
     temp3.push(product.Category)
    // console.log("========11111", temp3)
  return (
    <div className="product-details-content ml-70">
      <h2>{product.title}</h2>
      <div className="product-details-price">
        {discountedPrice !== null ? (
          <Fragment>
            <span>{currency.currencySymbol + finalDiscountedPrice}</span>{" "}
            <span className="old">
              {currency.currencySymbol + finalProductPrice}
            </span>
          </Fragment>
        ) : (
          <span>{currency.currencySymbol + finalProductPrice} </span>
        )}
      </div>
        <div className="pro-details-rating-wrap">
          <div className="pro-details-rating">
            <Rating ratingValue={product.rating} />
          </div>
        </div>
      <div className="pro-details-list">
        <p>{product.shortDescription}</p>
      </div>
    <div>

        <div className="pro-details-size-color">
          <div className="pro-details-size">
            <span>Color</span>
          <div className="pro-details-size-content">
                   { temp2.flat().map((item) => { 
                      // console.log("first222",item)
                        return (
                          <label
                            className={`pro-details-size-content--single`}
                            label={item.name}
                            >
                            <input
                              type="radio"
                              value={item.name}
                              checked={
                                item.name === selectedProductColor
                                ? "checked"
                                : ""
                              }
                              onChange={() => {
                                setSelectedProductColor(item.name);
                                setQuantityCount(1);
                              }}
                            />
                            <span className="size-name">{item.name}</span>
                          </label>
                        );
                })}
            </div>              
            
          </div>
          <div className="pro-details-size">
            <span>Size</span>
            <div className="pro-details-size-content">
                   { temp1.flat().map((item) => { 
                      // console.log("first222",item)
                        return (
                          <label
                            className={`pro-details-size-content--single`}
                            label={item.name}
                            >
                            <input
                              type="radio"
                              value={item.name}
                              checked={
                                item.name === selectedProductSize
                                ? "checked"
                                : ""
                              }
                              onChange={() => {
                                setSelectedProductSize(item.name);
                                setProductStock(item.stock);
                                setQuantityCount(1);
                              }}
                            />
                            <span className="size-name">{item.name}</span>
                          </label>
                        );
                })}
            </div>
          </div>
        </div>
        
    </div>
    
      {product.affiliateLink ? (
        <div className="pro-details-quality">
          <div className="pro-details-cart btn-hover ml-0">
            <a
              href={product.affiliateLink}
              rel="noopener noreferrer"
              target="_blank"
            >
              Buy Now
            </a>
          </div>
        </div>
      ) : (
        <div className="pro-details-quality">
          <div className="cart-plus-minus">
            <button
              onClick={() =>
                setQuantityCount(quantityCount > 1 ? quantityCount - 1 : 1)
              }
              className="dec qtybutton"
            >
              -
            </button>
            <input
              className="cart-plus-minus-box"
              type="text"
              value={quantityCount}
              readOnly
            />
            <button
              onClick={() =>
                setQuantityCount(
                  quantityCount < product.stock - productCartQty
                    ? quantityCount + 1
                    : quantityCount
                )
              }
              className="inc qtybutton"
            >
              +
            </button>
          </div>
          <div className="pro-details-cart btn-hover">
            {product.stock && product.stock > 0 ? (
              <button
                onClick={() =>
                  dispatch(addToCart({
                    ...product,
                    quantity: quantityCount,
                    selectedProductColor: selectedProductColor ? selectedProductColor : product.selectedProductColor ? product.selectedProductColor : null,
                    selectedProductSize: selectedProductSize ? selectedProductSize : product.selectedProductSize ? product.selectedProductSize : null
                  }))
                }
                disabled={productCartQty >= productStock}
              >
                {" "}
                Add To Cart{" "}
              </button>
            ) : (
              <button disabled>Out of Stock</button>
            )}
          </div>
          <div className="pro-details-wishlist">
            <button
              className={wishlistItem !== undefined ? "active" : ""}
              disabled={wishlistItem !== undefined}
              title={
                wishlistItem !== undefined
                  ? "Added to wishlist"
                  : "Add to wishlist"
              }
              onClick={() => dispatch(addToWishlist(product))}
            >
              <i className="pe-7s-like" />
            </button>
          </div>
          
        </div>
      )}
        <div className="pro-details-meta">
          <span>Categories :</span>
          <ul>
            {temp3?.flat().map((it) => {
              return (
                <div>
                  {it?.name}
                </div>
              );
            })}
          </ul>
        </div>
      {product.tag ? (
        <div className="pro-details-meta">
          <span>Tags :</span>
          <ul>
            {product.tag.map((single, key) => {
              return (
                <li key={key}>
                  <Link to={process.env.PUBLIC_URL + "/shop-grid-standard"}>
                    {single}
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      ) : (
        ""
      )}

      <div className="pro-details-social">
        <ul>
          <li>
            <a href="//facebook.com">
              <i className="fa fa-facebook" />
            </a>
          </li>
          <li>
            <a href="//dribbble.com">
              <i className="fa fa-dribbble" />
            </a>
          </li>
          <li>
            <a href="//pinterest.com">
              <i className="fa fa-pinterest-p" />
            </a>
          </li>
          <li>
            <a href="//twitter.com">
              <i className="fa fa-twitter" />
            </a>
          </li>
          <li>
            <a href="//linkedin.com">
              <i className="fa fa-linkedin" />
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

ProductDescriptionInfo.propTypes = {
  cartItems: PropTypes.array,
  compareItem: PropTypes.shape({}),
  currency: PropTypes.shape({}),
  discountedPrice: PropTypes.number,
  finalDiscountedPrice: PropTypes.number,
  finalProductPrice: PropTypes.number,
  product: PropTypes.shape({}),
  wishlistItem: PropTypes.shape({})
};

export default ProductDescriptionInfo;
